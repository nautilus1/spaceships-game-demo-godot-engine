tool
extends GraphEdit


var free: bool = false
var node_res = load("res://addons/level_states_editor/nodo/node.tscn")
var state_index: int = -1

var nodes: Array = []

func _ready() -> void:
	add_valid_connection_type(0, 0)


#func _input(event):
#	if Input.is_action_just_pressed("Control"):
#		new_node()


func new_node():
	var new_node: GraphNode = node_res.instance()
	state_index += 1
	new_node.connect("close_request", self, "close_node", [new_node, state_index])
	add_child(new_node)
	new_node.state_index = state_index
	new_node.set_slot( 0, true, 0, Color.white, true, 0, Color.blueviolet, null, null)
	new_node.offset = get_node("local_mouse").get_local_mouse_position() + scroll_offset
	
	nodes.insert(state_index, new_node)


func _on_editor_connection_request(from, from_slot, to, to_slot) -> void:
	connect_node(from, from_slot, to, to_slot)


func close_node(from, s_index_n_from):
	var last_node: GraphNode
	var next_node: GraphNode
	
	for new_node_child in get_children():
		if not new_node_child is GraphNode:
			continue
		
		if new_node_child.state_index == s_index_n_from - 1:
			last_node = new_node_child
			disconnect_node(last_node.name, 0, from.name, 0)
			continue
		elif new_node_child.state_index > s_index_n_from:
			next_node = new_node_child
			nodes.remove(state_index)
			new_node_child.state_index -= 1
			nodes.insert(state_index, new_node_child)
			if {"from_port": 0, "from": from.name, "to_port": 0, "to": next_node.name} in get_connection_list():
				disconnect_node(from.name, 0, next_node.name, 0)
			continue
	
	state_index -= 1
	from.queue_free()
	nodes.remove(s_index_n_from)
