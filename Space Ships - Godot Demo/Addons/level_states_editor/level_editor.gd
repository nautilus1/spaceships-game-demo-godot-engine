tool
extends EditorPlugin

const MainPanel = preload("res://addons/level_states_editor/level_editor_interface.tscn")

var main_panel_instance


func _enter_tree():
	set_process_input(true)
	main_panel_instance = MainPanel.instance()
	add_control_to_bottom_panel(main_panel_instance, "Editor de Niveles")


func _input(event):
	if Input.is_action_just_pressed("ui_down"):
		main_panel_instance.get_node("editor").new_node()

func has_main_screen():
	return true

func get_plugin_name():
	return "Editor de Niveles"


func _exit_tree():
	remove_control_from_bottom_panel(main_panel_instance)
	main_panel_instance.queue_free()
	set_process_input(false)
