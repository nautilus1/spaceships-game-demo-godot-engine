tool
extends Control
 
var states_list: Array
var directory_levels = "res://Levels/levels_generated"

onready var editor_states_nodes: GraphEdit = $editor


func _on_Button_pressed() -> void:
	var states_nodes = editor_states_nodes.nodes
	
	for state_index in states_nodes:
		states_list.append(state_index.get_data())
	
	save_scene()


func save_scene() -> void:
	var current_name: String = "level_0"
	var num: int = 0
	
	var directory: Directory = Directory.new()
	
	if directory.open(directory_levels) == OK:
		directory.list_dir_begin()
		var file_name = directory.get_next()
		while file_name != "":
			if file_name == current_name:
				directory.list_dir_begin()
				num += 1
				current_name = "level_" + str(num)
			
			file_name = directory.get_next()
		directory.make_dir_recursive("res://Levels/levels_generated/%s" % [current_name])
	
	var file_save = File.new()
	file_save.open("res://Levels/levels_generated/%s/%s.scn.save" % [current_name, current_name], File.WRITE)
	file_save.store_line(to_json(states_list))
	file_save.close()
	
	var scene = PackedScene.new()
	var new_scene_pkg = load("res://Levels/Level.tscn").instance()
	scene.pack(new_scene_pkg)
	ResourceSaver.save("res://Levels/levels_generated/%s/%s.scn" % [current_name, current_name], scene)
