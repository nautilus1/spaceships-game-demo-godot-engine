tool
extends GraphNode

var state_index: int = 0 setget set_state

var state_name: String = ""
var state_param: int

onready var select_value_node = $select_value


func set_state(state_i) -> void:
	state_index = state_i
	$state_num.text = "INDICE DEL ESTADO %s" % str(state_index)


func get_data() -> Dictionary:
	return {"state": state_name, "param": state_param}


func _on_states_type_selected(state) -> void:
	state_name = state
	
	if state_name == "asteroids":
		select_value_node.text = "¿CUÁNTO TIEMPO?"
		select_value_node.visible = true
	elif state_name == "enemies":
		select_value_node.text = "¿CUÁNTOS ENEMIGOS?"
		select_value_node.visible = true
	elif state_name == "boss":
		select_value_node.text = "JEFE"
		select_value_node.visible = false
	else:
		select_value_node.visible = false
		state_name = ""


func _on_select_value_value_changed(value) -> void:
	state_param = value
