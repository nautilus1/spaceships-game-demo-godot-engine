tool
extends HBoxContainer

signal value_changed(value)

var text: String = "" setget set_text


func _ready() -> void:
	$value.connect("text_entered", self, "new_text")


func set_text(new_text) -> void:
	text = new_text
	$propiedad.text = text


func _on_value_text_entered(new_value: String) -> void:
	var num = float(new_value)
	
	if num == 0:
		pass
	else:
		emit_signal("value_changed", num)
