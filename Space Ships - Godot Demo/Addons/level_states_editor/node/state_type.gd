tool
extends HBoxContainer

signal type_selected

var type_selected: String = ""


func _ready():
	$asteroids.connect("pressed", self, "select_type")
	$enemies.connect("pressed", self, "select_type")
	$boss.connect("pressed", self, "select_type")


func select_type() -> void:
	if type_selected == "":
		for i in get_children():
			if i.pressed:
				type_selected = i.name
				emit_signal("type_selected", i.name)
				continue
			else:
				i.disabled = true
	else:
		for i in get_children():
			i.disabled = false
			type_selected = ""
			emit_signal("type_selected", "")
