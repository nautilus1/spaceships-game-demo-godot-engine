extends RigidBody2D


func _ready() -> void:
	$End.stream_paused = false if Globals.sfx else true


func _on_detectar_jugador_body_entered(body):
	if body and body.is_in_group("Players"):
		if !body.lifes <= 0 and body.state_machine.current_state.name != "Hit":
			body.state_machine.change_state("Hit")
		else:
			destruir()


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func sfx_destroy() -> void:
	$End.playing = true if Globals.sfx else false


func destruir():
#	owner.get_node("Camera2D").add_trauma(0.12)
	Globals.disable_collision_layer(self)
	Globals.disable_collision_layer($detectar_jugador)
	Globals.score += 1
	$Animation.play("Explosion")
