extends Area2D

export var velocidad = 10

var direccion = Vector2()
var gun: String
var anim_name: String setget set_anim_name


func _ready() -> void:
	$Start.playing = true if Globals.sfx else false


func _process(delta):
	if direccion.x and direccion.y:
		direccion.x = 0
		direccion.y = 0
	
	if direccion.x:
		move_local_x(direccion.x * velocidad * delta)
		$Sprite.scale.x = -direccion.x
	elif direccion.y:
		move_local_y(direccion.y * velocidad * delta)
		$Sprite.scale.y = -direccion.y


func set_anim_name(new_anim_name) -> void:
	anim_name = new_anim_name
	$AnimationPlayer.play(anim_name)


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _on_Bala_body_entered(body):
	if body && body.is_in_group("Asteroides"):
		body.destruir()
		queue_free()
	elif body && self.gun != "Player" && body.is_in_group("Players"):
		if body.state_machine.current_state.name != "Hit" and !body.lifes <= 0:
			body.state_machine.change_state("Hit")
	elif body && self.gun != "Enemy" && body.is_in_group("Enemies"):
		if body.state_machine.current_state.name != "Hit" and !body.lifes <= 0:
			body.state_machine.change_state("Hit", true)
