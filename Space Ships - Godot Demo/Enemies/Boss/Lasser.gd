extends State

export var laser_node_path: NodePath


func enter(current_state) -> void:
	get_node(laser_node_path).active_laser()


func physics(delta) -> void:
	move(delta, direction_move)


func handle_input(event) -> void:
	pass


func exit() -> void:
	get_node(laser_node_path).desactive_laser()
	direction_move = Vector2.ZERO
	character.direction = Vector2.ZERO
