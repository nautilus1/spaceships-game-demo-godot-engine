extends State


func enter(current_state) -> void:
	pass


func physics(delta) -> void:
	move(delta, direction_move)


func handle_input(event) -> void:
	pass


func exit() -> void:
	direction_move = Vector2.ZERO
	character.direction = Vector2.ZERO
