extends State

export (float, 0.0, 1.9) var time_to_teleport


func enter(current_state) -> void:
	randomize()
	teleport()


func handle_input(event) -> void:
	pass


func teleport() -> void:
	anim_character.play("Teletransport")
	
	yield(get_tree().create_timer(time_to_teleport), "timeout")
	
	anim_character.play_backwards("Teletransport")
	
	character.global_position.x = rand_range(10, 700)
	character.global_position.y = rand_range(0, 150)
	
	emit_signal("state_finished")
