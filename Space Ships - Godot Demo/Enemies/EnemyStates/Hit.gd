extends State


func enter(state_current) -> void:
	anim_character.play("Hit")
	character.lifes -= 1
	yield(anim_character, "animation_finished")
	emit_signal("state_finished")


func physics(delta) -> void:
	get_direction()
	move(delta)
