extends State

export var shoot_timer_path: NodePath
export var character_pivot_path: NodePath

var shoot_timer: Timer

var bullet = load("res://Bullet/bullet.tscn")


func enter(state_current) -> void:
	anim_character.play("Move")
	shoot_timer = get_node(shoot_timer_path)


func physics(delta) -> void:
	move(delta, direction_move)
	
	if shoot_timer.is_stopped():
		shoot()
		shoot_timer.start()


func shoot() -> void:
	for i in ["", "2"]:
		var new_bullet = bullet.instance()
		new_bullet.anim_name = "bullet_03"
		new_bullet.gun = "Enemy"
		character.get_parent().add_child(new_bullet)
		new_bullet.direccion.y = 1
		new_bullet.position = get_node(str(character_pivot_path) + "/pos_bullet%s" % i).global_position


func handle_input(event) -> void:
	pass


func exit() -> void:
	direction_move = Vector2.ZERO
	character.direction = Vector2.ZERO
