extends State


func enter(current_state: Node) -> void:
	anim_character.play(current_state.name)
	Globals.disable_collision_layer(character)
	yield(anim_character, "animation_finished")
	state_machine.set_active(false)
	character.emit_signal("died")
	character.queue_free()
