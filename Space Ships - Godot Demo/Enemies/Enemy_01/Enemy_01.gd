extends KinematicBody2D

signal damaged
signal died
signal patron_completed

export var speed: float = 500.00

var direction := Vector2();
var velocity := Vector2();

var bullet = load("res://Bullet/bullet.tscn")
var lifes = 2 setget set_lifes

onready var direction_change_timer: Timer = $change_direction
onready var teletransport_timer: Timer = $time_to_teleport
onready var shoot_timer: Timer = $time_to_shoot

onready var state_machine = $StateMachine
onready var animation_player = $Animation

export var patron_attack_01: Array = [
	{"state": "Shoot", "move_to": Vector2(60, 0), "direction": Vector2(-1, 0)},
	{"state": "Shoot", "move_to": Vector2(300, 0), "direction": Vector2(1, 1)},
	{"state": "Move", "move_to": Vector2(500, 0), "direction": Vector2(1, 0)}
]

export var sequence_attacks_list: Array = [
	"attack_01"
]

var patron_current_state: Dictionary = {}
var sequence_pause: bool = false
var index_state = 0


func _ready() -> void:
	state_machine.connect("patron_changed", self, "patron_history")
	state_machine.connect("continue_patron", self, "continue_patron")
	
	randomize()
	state_machine.start()
	
	set_process(false)
	
	sequence_attacks()
#	attack_01()
#	attack_with_laser()


#func _input(event: InputEvent) -> void:
#	if Input.is_action_just_pressed("ui_accept"):
#		state_machine.change_state("Hit", true)


func sequence_attacks() -> void:
	var temp_index = 0
	
	while index_state != 4:
		for patron in sequence_attacks_list:
			call(patron)
			index_state += 1
			yield(state_machine, "states_runed")
		
		if index_state > sequence_attacks_list.size() - 1:
			index_state = 0


func attack_01():
	state_machine.run_states(patron_attack_01)
	yield(state_machine, "patron_changed")


func _physics_process(delta) -> void:
	apply_gravity(delta)


func apply_gravity(delta) -> void:
	velocity.y = speed * direction.y
	velocity.x = speed * direction.x
	
	move_and_slide(velocity, Vector2(0, -1))
	
	if global_position.x < 0:
		global_position.x = 699
	elif global_position.x > 700:
		global_position.x = 1
	
	if global_position.y > 800:
		global_position.y = 0
	elif global_position.y < 0:
		global_position.y = 0


func set_lifes(new_lifes) -> void:
	lifes = new_lifes
	
	if lifes <= 0:
		state_machine.change_state("Dead")
		return
	
	emit_signal("damaged", lifes)


func patron_history(patron_i) -> void:
	patron_current_state = patron_i


func continue_patron() -> void:
	state_machine.continue_run_failed_state(patron_current_state)
