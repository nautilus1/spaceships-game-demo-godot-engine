extends Control


export var score_current_node_dir: NodePath
export var score_max_node_dir: NodePath

var score_current_node = null
var score_max_node = null


func _ready() -> void:
	score_current_node = get_node(score_current_node_dir)
	score_max_node = get_node(score_max_node_dir)
	update_score()


func update_score() -> void:
	score_current_node.text = str(Globals.score)
	score_max_node.text = str(Globals.max_score)


func _on_reload_pressed():
	get_tree().reload_current_scene()


func _on_exit_pressed():
	get_tree().quit()
