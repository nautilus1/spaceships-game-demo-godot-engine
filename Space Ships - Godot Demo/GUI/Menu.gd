extends Control

export var music_button_dir: NodePath
export var sfx_button_dir: NodePath

var music_button: CheckBox = null
var sfx_button: CheckBox = null


func _ready() -> void:
	music_button = get_node(music_button_dir)
	sfx_button = get_node(sfx_button_dir)
	
	music_button.connect("pressed", self, "music_pressed")
	sfx_button.connect("pressed", self, "sfx_pressed")


func _process(delta: float) -> void:
	$ParallaxBackground/ParallaxLayer.motion_offset.y += 420 * delta


func music_pressed():
	if music_button.pressed:
		Globals.change_music(true)
	else:
		Globals.change_music(false)

func sfx_pressed():
	if sfx_button.pressed:
		Globals.change_sfx(true)
	else:
		Globals.change_sfx(false)
