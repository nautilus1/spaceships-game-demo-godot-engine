extends NinePatchRect

signal color_changed
signal window_changed(window)

onready var anim: Tween = $Tween

var into: bool = false

export var anim_duration := 0.4
export var color_gray: Color = Color("#737373")


func _ready() -> void:
	gray()
	set_process(false)
	set_process_input(false)


func _input(event: InputEvent) -> void:
	if event is InputEventMouse and event.is_pressed():
		emit_signal("window_changed", self)
		set_process_input(false)

func _process(delta):
	var rect_to_mouse = Rect2(rect_global_position.x, rect_global_position.y, rect_size.x, rect_size.y)
	
	if rect_to_mouse.has_point(get_global_mouse_position()):
		into = true
		return
	else:
		move_down()
		set_process(false)
		into = false


func window_out() -> void:
	anim.interpolate_property(self, "rect_position", Vector2(150, 200), Vector2(-303.936, 200), anim_duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	anim.start()
	yield(anim, "tween_completed")
	gray()


func window_in() -> void:
	anim.interpolate_property(self, "rect_position", Vector2(150, -200), Vector2(150, 200), anim_duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	anim.start()
	white()


func move_left() -> void:
	anim.interpolate_property(self, "rect_position", Vector2(-500, 200), Vector2(-303.936, 200), anim_duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	anim.start()


func move_right() -> void:
	anim.interpolate_property(self, "rect_position", Vector2(700, 200), Vector2(584.362, 200), anim_duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	anim.start()


func move_up() -> void:
	anim.interpolate_property(self, "rect_position", Vector2(rect_position.x, 200), Vector2(rect_position.x, 160), anim_duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	anim.start()


func move_down() -> void:
	anim.interpolate_property(self, "rect_position", Vector2(rect_position.x, 160), Vector2(rect_position.x, 200), anim_duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	anim.start()


func gray() -> void:
	modulate = color_gray


func white() -> void:
	modulate = Color.white


func _on_Window_mouse_entered() -> void:
	set_process_input(true)
	white()
	emit_signal("color_changed", self)


func _on_Window_window_changed() -> void:
	set_process_input(false)


func _on_Window_mouse_exited() -> void:
	set_process_input(false)
