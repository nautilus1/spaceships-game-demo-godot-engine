extends Control

onready var window_current = $window_start


func _ready() -> void:
	for i in get_children():
		i.connect("color_changed", self, "color_gray_update")
		i.connect("window_changed", self, "change_window_direction")
	
	$windows_options.move_right()
	$windows_credits.move_left()


func change_window_direction(window):
	if window.name == window_current.name:
		return
	
	if get_global_mouse_position().x < 300:
		last_window()
	else:
		next_window()


func change_window(new_window) -> void:
	if !new_window.anim.is_active():
		new_window.window_in()
	
	window_current = new_window
	get_window_left_of_current()
	get_window_right_of_current()


func next_window():
	move_child(window_current, window_current.get_position_in_parent() + get_child_count() - 1)
	for window in get_children():
		if window.get_position_in_parent() == 0:
			change_window(window)
			break


func get_window_right_of_current():
	get_child(window_current.get_position_in_parent() + 1).move_right()


func get_window_left_of_current():
	get_child(get_child_count() - 1).move_left()


func last_window():
	for window in get_children():
		if window.get_position_in_parent() == get_child_count() - 1:
			move_child(window, 0)
			change_window(window)
			break


func color_gray_update(window) -> void:
	for i in get_children():
		if i == window:
			continue
		i.gray()


func _on_ButtonOrangeBigup_pressed():
	get_tree().change_scene("res://Game.tscn")
