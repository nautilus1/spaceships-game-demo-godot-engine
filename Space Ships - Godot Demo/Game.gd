extends Node
class_name Game

export var heart_bar_path: NodePath

var sfx: bool = true

onready var game_over = $GameOver
#onready var puntos = $GUI/HUD_container/Contenedor_Puntos


func _ready():
	Globals.connect("music", self, "active_music")
	Globals.connect("sfx", self, "active_sfx")
	
	active_music(Globals.music)
	$Levels.get_child(0).owner = self


func change_heart_bar(new_value) -> void:
	var heart_bar: Range = get_node(heart_bar_path)
	heart_bar.value = new_value / 0.04
	

func active_music(value: bool) -> void:
	$Music.stop() if not value else $Music.play()
