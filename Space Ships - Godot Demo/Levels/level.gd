extends Node2D

signal state_finished

var level_states: Array = [{"state": "", "param": {} }] setget set_level_states

var level_states_max: int = 0
var index_state: int = 0

onready var obstacles_control_node = $Asteroids
onready var enemies_control_node = $Enemies
onready var screen_effects = $ScreenEffects

onready var player = $Jugador


func _ready() -> void:
	load_level_states()
	level_states_max = level_states.size()
	
	player.connect("died", self, "end_game")
	player.connect("damaged", owner, "change_heart_bar")


func _process(delta):
	$ParallaxBackground/ParallaxLayer.motion_offset.y += 420 * delta


func load_level_states():
	var load_file = File.new()
	load_file.open(filename + ".save", File.READ)
	
	while load_file.get_position() < load_file.get_len():
		level_states = parse_json(load_file.get_line())
	
	load_file.close()
	
# Probar con diccionarios
func state_asteroids(time) -> void:
	screen_effects.warning_asteroids()
	yield(screen_effects, "animation_finished")
	obstacles_control_node.start_asteroids(time)
	yield(obstacles_control_node, "finished")
	obstacles_control_node.stop_asteroids()
	emit_signal("state_finished")


func state_enemies(size) -> void:
	screen_effects.warning_enemies()
	yield(screen_effects, "animation_finished")
	enemies_control_node.start_enemies(size)
	yield(enemies_control_node, "finished")
	enemies_control_node.stop_enemies()
	emit_signal("state_finished")


func state_boss() -> void:
	screen_effects.warning_boss()
	yield(screen_effects, "animation_finished")
	enemies_control_node.start_boss()
	yield(enemies_control_node, "enemy_changed")
	emit_signal("state_finished")


func sequence_level() -> void:
	while index_state < level_states_max:
		for state in level_states:
			if level_states[index_state].state != "boss":
				call("state_" + level_states[index_state].state, level_states[index_state].param)
			else:
				call("state_" + level_states[index_state].state)
			index_state += 1
			yield(self, "state_finished")


func start_game() -> void:
	player.start_game()
	yield(get_tree().create_timer(4.1), "timeout")
	sequence_level()


func set_level_states(new_dictionary: Array) -> void:
	level_states = new_dictionary


func end_game() -> void:
	var new_game_over = load("res://GUI/GameOver.tscn").instance()
	owner.game_over.add_child(new_game_over)
