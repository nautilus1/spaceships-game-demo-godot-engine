extends State


func enter(current_state):
	anim_character.play("Player_center")


func physics(delta):
	get_direction()
	
	if direction.x != 0 || direction.y != 0:
		change_state("Move")
