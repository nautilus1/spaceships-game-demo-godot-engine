extends State


func enter(current_state) -> void:
	anim_character.play("Hit")
	character.lifes -= 1
	
	if character.lifes <= 0:
		state_machine.change_state("Died")
		return
	
	yield(anim_character, "animation_finished")
	change_state("Idle")


func physics(delta) -> void:
	get_direction()
	move(delta)
