extends State


func enter(current_state) -> void:
#	anim_character.play("Player_left")
	pass


func physics(delta):
	get_direction()
	
	if direction.x == 0 && direction.y == 0:
		change_state("Idle")
	
	move(delta)
