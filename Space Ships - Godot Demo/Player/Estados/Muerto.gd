extends State


func enter(current_state):
	anim_character.play("Dead")
	died()


func died():
	character.set_physics_process(false)
	character.set_process_input(false)
	character.state_machine._active = false
	character.emit_signal("died")
