extends KinematicBody2D

signal damaged # Esta señal se emite cuando el jugador recibe daño y debe cambiar la barra de vida.
signal died # Esta señal se emite cuando las vidas se han agotado y estamos muertos.

export var speed: float # "speed" es la aceleracion con la que puede moverse la nave. "float" indica que se trata de un numero con decimales.

var bullet_resource = load("res://Bullet/bullet.tscn") # Es la direccion de la escena "bala". Cargaremos una nueva escena "bala" cada vez que disparemos.
var direction: Vector2 = Vector2(0, 0) # Es la direccion de la nave en el eje horizontal y vertical.
var velocity: Vector2 = Vector2(0, 0) # Es la velocidad final que tiene la nave cuando calculamos su aceleracion con la direccion que sigue en el eje horizontal o vertical.
var lifes = 4 setget set_lifes

onready var state_machine = $StateMachine
onready var animation_player = $AnimationPlayer


func _ready() -> void:
	set_physics_process(false)
	start_game()


func start_game() -> void:
	state_machine.start()
	set_physics_process(true)


func _physics_process(delta) -> void:
	apply_gravity(delta)

	if Input.is_action_pressed("fire"):
		if $time_to_shoot.is_stopped():
			shoot()
			$time_to_shoot.start()


func shoot() -> void:
	for i in ["", "2"]:
		var new_bullet = bullet_resource.instance()
		new_bullet.anim_name = "bullet_02"
		new_bullet.gun = "Player"
		get_parent().add_child(new_bullet)
		new_bullet.direccion.y = -1
		new_bullet.position = get_node("pivot/pos_bullet%s" % i).global_position


func apply_gravity(delta) -> void:
	move_and_slide(velocity, Vector2(0, -1));
	
	if is_on_floor():
		velocity.y = 0;
	
	if is_on_ceiling():
		velocity.y = 0;


func set_lifes(new_lifes) -> void:
	lifes = new_lifes
	
	if lifes <= 0:
		Globals.disable_collision_layer(self)
		state_machine.change_state("Died")
	
	emit_signal("damaged", lifes)
