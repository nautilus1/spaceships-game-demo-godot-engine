extends Camera2D

export var decay: float = 0.8;

export var max_offset: Vector2 = Vector2(80, 80);
export var max_roll: float = 0.2;

var trauma = 0.0;
var trauma_power = 2;

var noise_y = 0;

onready var noise = OpenSimplexNoise.new();


func _ready() -> void:
	randomize();
	noise.seed = randi();
	noise.period = 4;


func add_trauma(amount: float) -> void:
	trauma = min(trauma + amount, 1.0);


func _process(delta) -> void:
	if trauma:
		trauma = max(trauma - (decay * delta), 0);
		shake();


func shake() -> void:
	var amount = pow(trauma, trauma_power);
	noise_y += 1;
	rotation = max_roll * amount * noise.get_noise_2d(noise.seed, noise_y);
	offset.x = max_offset.x * amount * noise.get_noise_2d(noise.seed * 2, noise_y);
	offset.y = max_offset.y * amount * noise.get_noise_2d(noise.seed * 3, noise_y);
