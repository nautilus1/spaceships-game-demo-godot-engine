extends Node
class_name State

onready var parent: Node = get_parent(); # StateMachine is the parent;
var character: KinematicBody2D = owner; # Player or NPc node is the Owner;

var anim_character: AnimationPlayer = null;
var self_anim_name: String = "";

var speed: float;

var velocity := Vector2();
var direction := Vector2();


func _ready() -> void:
	set_physics_process(false)
	set_process_input(false)


func enter() -> void:
	pass


func get_direction() -> void:
	if character.is_in_group("Players"):
		direction.x = int(Input.is_action_pressed("right"))-int(Input.is_action_pressed("left"))
		direction.y = int(Input.is_action_pressed("down"))-int(Input.is_action_pressed("up"))
		character.direction = direction
	
	if direction.x != 0:
		character.get_node("pivot").scale = Vector2(-direction.x, 1);


func move(delta) -> void:
	character.velocity.x = character.speed * direction.x
	character.velocity.y = character.speed * direction.y


func exit() -> void:
	set_physics_process(false)
	set_process_input(false)
	character.velocity.x = 0;
