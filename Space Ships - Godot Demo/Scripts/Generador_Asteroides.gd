extends Node2D

signal finished

onready var time_to_instantiate: Timer = $time_to_instantiate


func _ready():
	randomize()
	

func start_asteroids(time_to_exit) -> void:
	instance_asteroid()
	yield(get_tree().create_timer(time_to_exit), "timeout")
	emit_signal("finished")


func instance_asteroid(time_active: float = time_to_instantiate.wait_time):
	var new_asteroid = load("res://Asteroid/Asteroid.tscn").instance()
	add_child(new_asteroid)
	new_asteroid.global_position.y = -10
	new_asteroid.global_position.x = rand_range(100, 650)
	new_asteroid.owner = owner
	time_to_instantiate.start(time_active)


func stop_asteroids() -> void:
	time_to_instantiate.stop()
