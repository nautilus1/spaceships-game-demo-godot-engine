extends Node2D

signal finished
signal enemy_changed

export var enemies_max_on_screen: int = 3
var enemies_on_screen: int = 0
var enemies_to_instance: int = 0

var enemies_list: Array = []

onready var time_to_inst = $time_to_instantiate


func start_boss() -> void:
	instance_boss()


func start_enemies(size) -> void:
	instance_enemy(size)
	enemies_to_instance = size


func kill_count():
	enemies_list.pop_back()
	enemies_on_screen -= 1
	
	if enemies_on_screen <= 0:
		emit_signal("finished")
		return
	
	emit_signal("enemy_changed")


func instance_boss() -> void:
	var new_boss = load("res://Enemies/Boss/Boss.tscn").instance()
	add_child(new_boss)
	new_boss.global_position.y = 10
	new_boss.global_position.x = rand_range(100, 650)
	new_boss.owner = owner
	new_boss.connect("died", self, "kill_count")
	enemies_on_screen += 1
	enemies_list.append(new_boss.name)


func instance_enemy(size) -> void:
	for enemy in size:
		time_to_inst.start()
		yield(time_to_inst, "timeout")
		
		if enemies_on_screen == enemies_max_on_screen:
			yield(self, "enemy_changed")
		
		enemies_on_screen += 1
		var new_enemy = load("res://Enemies/Enemy_01/Enemy_01.tscn").instance()
		add_child(new_enemy)
		enemies_list.append(new_enemy.name)
		new_enemy.global_position.y = -10
		new_enemy.global_position.x = rand_range(100, 650)
		new_enemy.owner = owner
		new_enemy.connect("died", self, "kill_count")


func stop_enemies() -> void:
	time_to_inst.stop()
