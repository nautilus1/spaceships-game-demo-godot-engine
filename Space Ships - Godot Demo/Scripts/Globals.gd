extends Node

signal scoreboard
signal music
signal sfx

var music: bool = true
var sfx: bool = true

var score: int setget set_score, get_score
var max_score: int


func get_score() -> int:
	if max_score < score:
		max_score = score
		save_max_score()
	else:
		load_max_score()
	
	return score


func load_max_score() -> void:
	var file_load: File = File.new()
	file_load.open("res://Save/score.save", File.READ)
	max_score = int(file_load.get_line())


func save_max_score() -> void:
	var file_save: File = File.new()
	file_save.open("res://Save/score.save", File.WRITE)
	file_save.store_line(str(max_score))
	file_save.close()


func set_score(new_score) -> void:
	score = new_score
	emit_signal("scoreboard")


func disable_collision_layer(node) -> void:
	node.set_collision_layer_bit(0, false)
	node.set_collision_mask_bit(0, false)


func change_music(value: bool) -> void:
	music = value
	emit_signal("music", music)


func change_sfx(value: bool) -> void:
	sfx = value
	emit_signal("sfx", sfx)
