extends HBoxContainer


var numeros_img = {}

var num_img = [
		"res://assets/Spaceship UI Template/PNG/0 with shadow.png",
		"res://assets/Spaceship UI Template/PNG/1 with shadow.png",
		"res://assets/Spaceship UI Template/PNG/2 with shadow.png",
		"res://assets/Spaceship UI Template/PNG/3 with shadow.png",
		"res://assets/Spaceship UI Template/PNG/4 with shadow.png",
		"res://assets/Spaceship UI Template/PNG/5 with shadow.png",
		"res://assets/Spaceship UI Template/PNG/6 with shadow.png",
		"res://assets/Spaceship UI Template/PNG/7 with shadow.png",
		"res://assets/Spaceship UI Template/PNG/8 with shadow.png",
		"res://assets/Spaceship UI Template/PNG/9 with shadow.png"
		]


func _ready() -> void:
	num(999990)


func num(cifra: int) -> void:
	var digits = []
	
	for d in str(cifra):
		digits.append(d)
	
	while digits.size() > get_children().size() - 1:
		var new_texture_rect = TextureRect.new()
		add_child(new_texture_rect)
		new_texture_rect.expand = true
		new_texture_rect.rect_size = $TextureRect.rect_size
		new_texture_rect.rect_min_size = $TextureRect.rect_min_size
	
	for i in get_children():
		for d in digits:
			if i is TextureRect:
				i.texture = load(num_img[int(d)])
				digits.erase(d)
				break
	
	#get_parent().get_parent().set_margins_preset(Control.PRESET_CENTER)
	
