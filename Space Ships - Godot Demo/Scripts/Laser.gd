extends Node2D

var max_lenght: float = 800
var lenght: Vector2 = Vector2(200, 0)

onready var line = $line
onready var raycast = $raycast
onready var tween = $effect


func active_laser() -> float:
	tween.interpolate_property(raycast, 
		"cast_to", Vector2(0, 0), lenght.normalized() * max_lenght,
		0.25, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	
	return 0.25


func desactive_laser() -> float:
	tween.interpolate_property(raycast, 
		"cast_to", lenght.normalized() * max_lenght, Vector2(0, 0),
		0.25, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	
	return 0.25


func _physics_process(delta):
	line.points[1].x = raycast.cast_to.length()
	
	if raycast.is_colliding():
		line.points[1].x = raycast.get_collision_point().length()
	
	line.rotation_degrees = 90
