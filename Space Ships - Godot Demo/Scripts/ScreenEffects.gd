extends CanvasLayer

signal animation_finished

onready var animation = $Animation


func warning_asteroids() -> void:
	animation.play("warning_asteroids")
	yield(animation, "animation_finished")
	emit_signal("animation_finished")


func warning_enemies() -> void:
	animation.play("warning_enemies")
	yield(animation, "animation_finished")
	emit_signal("animation_finished")


func warning_boss() -> void:
	animation.play("warning_boss")
	yield(animation, "animation_finished")
	emit_signal("animation_finished")
