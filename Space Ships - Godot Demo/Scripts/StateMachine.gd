extends Node

# - >| Finite State Machine |

var character: KinematicBody2D = owner;
var last_direction: Vector2;

var states: Dictionary = {};

var state_current: Node;
var state_history: Array;

var _active: bool = false setget set_active;


func _ready() -> void:
	set_physics_process(false)
	set_process_input(false)


func start() -> void:
	set_active(true)
	get_states_nodes()
	change_state("Detenido")


func get_states_nodes() -> void:
	for state_node in get_children():
		if state_node.get_children().size() > 0:
			for other_node in state_node.get_children():
				if other_node in states:
					continue
				
				states[other_node.name] = other_node
				states[other_node.name].character = owner
				states[other_node.name].parent = self
				states[other_node.name].anim_character = owner.animation_player
		
		if state_node in states:
			continue
		
		states[state_node.name] = state_node;
		states[state_node.name].character = owner;
		states[state_node.name].anim_character = owner.animation_player;


func _input(event) -> void:
	state_current.set_process_input(true)


func _physics_process(delta) -> void:
	state_current.set_physics_process(true)


func change_state(state_name) -> void:
	if !_active:
		return
	
	if state_name in states:
		if state_current:
			state_current.exit()
		
		state_current = states[state_name]
		
		if not state_current in state_history:
			state_history.push_front(state_current)
		elif state_current in state_history:
			state_history.erase(state_current)
			state_history.push_front(state_current)
		
		state_current.enter(state_current);
	
	if state_history.size() > 3:
		state_history.pop_back()


func set_active(value) -> void:
	_active = value
	set_physics_process(value)
	set_process_input(value)
	
	if !_active:
		state_current.exit()
		state_current = null
