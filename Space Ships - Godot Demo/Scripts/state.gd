extends Node
class_name State

signal move_finished
signal state_finished

var state_machine: StateMachine # StateMachine is the state_machine;
var character: KinematicBody2D = owner # Player or NPc node is the Owner;

var anim_character: AnimationPlayer = null
var self_anim_name: String = ""

var speed: float

var velocity := Vector2()
var direction := Vector2()

var direction_move: Vector2
var duration_move: float


func _ready() -> void:
	set_process(false)
	set_physics_process(false)
	set_process_input(false)


func enter(current_state: Node) -> void:
	if current_state.name in anim_character.get_animation_list():
		anim_character.play(current_state.name)


func handle_input(event) -> void:
	pass


func physics(delta) -> void:
	pass


func get_direction() -> void:
	if character.is_in_group("Players"):
		direction.x = int(Input.is_action_pressed("right"))-int(Input.is_action_pressed("left"));
		direction.y = int(Input.is_action_pressed("down"))-int(Input.is_action_pressed("up"))
		character.direction = direction;
	
	if direction.x != 0:
		character.get_node("pivot").scale = Vector2(direction.x, 1);


func move(delta, direction_move: Vector2 = character.direction) -> void:
	character.direction = direction_move
	if character.is_in_group("Players"):
		character.velocity = character.speed * character.direction


func exit() -> void:
	set_physics_process(false)
	set_process_input(false)
	character.velocity.x = 0;


func change_state(new_state) -> void:
	state_machine.change_state(new_state)


func set_info_move(list_info: Dictionary) -> void:
	duration_move = character.global_position.distance_to(Vector2(list_info["move_to"].x, character.global_position.y)) / character.speed
	direction_move = list_info["direction"]
	
	yield(get_tree().create_timer(duration_move), "timeout")
	
	if state_machine.current_state == self:
		emit_signal("move_finished")


func last_state() -> String:
	if state_machine.state_history.size() > 1:
		return state_machine.state_history[1].name
	return ""
