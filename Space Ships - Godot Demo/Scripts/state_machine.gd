extends Node
class_name StateMachine

signal state_changed
signal patron_changed
signal states_runed
signal continue_patron

# - >| Finite State Machine |

export var ready_state: NodePath

var character: KinematicBody2D = owner;
var last_direction: Vector2;

var states: Dictionary = {};

var state_history: Array
var states_to_run: Array

onready var current_state = get_node(ready_state)

var _active: bool = false setget set_active;


func start() -> void:
	get_states_nodes()
	set_active(true)
	change_state("Idle")


func get_states_nodes() -> void:
	for state_node in get_children():
		if state_node in states:
			continue
		
		states[state_node.name] = state_node
		states[state_node.name].state_machine = self
		states[state_node.name].character = owner
		states[state_node.name].anim_character = owner.animation_player


func _input(event) -> void:
	current_state.handle_input(event)


func _physics_process(delta) -> void:
	current_state.physics(delta)


func run_states(list_states: Array):
	for state in list_states:
		if state["state"] in ["Move", "Laser", "Shoot"]:
			change_state(state["state"])
			emit_signal("patron_changed", state)
			get_node(state["state"]).set_info_move(state)
			yield(get_node(state["state"]), "move_finished")
		else:
			change_state(state["state"])
			emit_signal("patron_changed", state)
			yield(get_node(state["state"]), "state_finished")
	
	return emit_signal("states_runed")


func continue_run_failed_state(state):
	change_state(state["state"])
	get_node(state["state"]).set_info_move(state)


func change_state(state_name, track: bool = false) -> void:
	if !_active:
		return
	
	if state_name in states:
		if current_state:
			current_state.exit()
		
		current_state = states[state_name]
		
		if not current_state in state_history:
			state_history.push_front(current_state)
		elif current_state in state_history:
			state_history.erase(current_state)
			state_history.push_front(current_state)
		
		current_state.enter(current_state);
		
		emit_signal("state_changed")
	
	if track:
		yield(current_state, "state_finished")
		emit_signal("continue_patron")
	
	if state_history.size() > 3:
		state_history.pop_back()


func set_active(value) -> void:
	_active = value
	set_physics_process(value)
	set_process_input(value)
	
	if !_active:
		current_state.exit()
		current_state = null
