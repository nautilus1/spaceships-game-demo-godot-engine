shader_type canvas_item;

void fragment() {
	vec2 uv = (SCREEN_UV - .5 * (1.0 / SCREEN_PIXEL_SIZE).xy) / (1.0 / SCREEN_PIXEL_SIZE).y;
	uv *= 2.;
	
	vec3 col = vec3(0);
	
	float d = length(uv);
//	float d = sqrt( pow(uv.x, 2) + pow(uv.y, 2) );
	float m = .05 / d;
	
	col += m;
	
	COLOR = vec4(col, 1.0);
}